<?php

namespace App\SingleResponsibilityPrinciple\Good;

use JetBrains\PhpStorm\Pure;

class CarBuilder
{
    private Car $car;

    #[Pure] public function __construct()
    {
    }

    public function build(string $model)
    {
        $this->car = new Car($model);
        echo $this->car.PHP_EOL;
        return $this->car;
    }
}
