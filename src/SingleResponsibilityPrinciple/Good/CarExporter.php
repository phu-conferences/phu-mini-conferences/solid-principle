<?php

namespace App\SingleResponsibilityPrinciple\Good;

class CarExporter
{
    public function __construct(
        private Car $car
    ) {
    }

    /**
     * @return string
     */
    public function exportCSV(): string
    {
        return 'Export information in CSV';
    }

    /**
     * @return string
     */
    public function exportHTML(): string
    {
        return 'Export information in HTML';
    }

    /**
     * @return string
     */
    public function exportXML(): string
    {
        return 'Export information in XML';
    }

    /**
     * @return string
     */
    public function exportExcel(): string
    {
        return 'Export information in Excel';
    }
}
