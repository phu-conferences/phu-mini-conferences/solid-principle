<?php

namespace App\SingleResponsibilityPrinciple\Good;

require_once '../../../init.php';

const MODEL_09_10_2021 = '007-super-car';

echo 'Car Builder'.PHP_EOL;
$carBuilder = new CarBuilder();
$car = $carBuilder->build(MODEL_09_10_2021);

echo '------------'.PHP_EOL;
echo 'Car Exporter'.PHP_EOL;
$carExporter = new CarExporter($car);
$carCSV = $carExporter->exportCSV();
echo $carCSV.PHP_EOL;
$carHTML = $carExporter->exportHTML();
echo $carHTML.PHP_EOL;
$carExcel = $carExporter->exportExcel();
echo $carExcel.PHP_EOL;
