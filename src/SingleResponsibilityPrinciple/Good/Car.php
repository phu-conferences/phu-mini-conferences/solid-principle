<?php

namespace App\SingleResponsibilityPrinciple\Good;

class Car
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Car model: '.$this->model;
    }
}
