<?php

namespace App\SingleResponsibilityPrinciple\Bad;

use JetBrains\PhpStorm\Pure;

class CarBuilder
{
    private Car $car;

    #[Pure] public function __construct()
    {
    }

    public function build(string $model)
    {
        $this->car = new Car($model);
        echo $this->car.PHP_EOL;
        return $this->car;
    }

    /**
     * @return string
     */
    public function exportCSV(): string
    {
        return 'Export information in CSV';
    }

    /**
     * @return string
     */
    public function exportHTML(): string
    {
        return 'Export information in HTML';
    }

    /**
     * @return string
     */
    public function exportXML(): string
    {
        return 'Export information in XML';
    }

    /**
     * @return string
     */
    public function exportExcel(): string
    {
        return 'Export information in Excel';
    }
}
