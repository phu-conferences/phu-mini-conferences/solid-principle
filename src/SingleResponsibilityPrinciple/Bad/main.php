<?php

namespace App\SingleResponsibilityPrinciple\Bad;

require_once '../../../init.php';

const MODEL_09_10_2021 = '007-super-car';

echo 'Car Builder'.PHP_EOL;
$carBuilder = new CarBuilder();
$carBuilder->build(MODEL_09_10_2021);
$carCSV = $carBuilder->exportCSV();
echo $carCSV.PHP_EOL;
$carHTML = $carBuilder->exportHTML();
echo $carHTML.PHP_EOL;
$carExcel = $carBuilder->exportExcel();
echo $carExcel.PHP_EOL;
