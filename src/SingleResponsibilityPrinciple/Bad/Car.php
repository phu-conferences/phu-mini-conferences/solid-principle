<?php

namespace App\SingleResponsibilityPrinciple\Bad;

class Car
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    public function __toString()
    {
        return 'Car model: '.$this->model;
    }
}
