<?php

namespace App\LiskovSubstitutionPrinciple\Good;

abstract class Vehicle
{
    public function __construct(
        protected string $model
    ) {
    }

    public function printModel(): string
    {
        return $this->model.PHP_EOL;
    }
}
