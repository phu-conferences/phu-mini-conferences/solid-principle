<?php

namespace App\LiskovSubstitutionPrinciple\Good;

use JetBrains\PhpStorm\Pure;

class Car extends Vehicle
{
    /**
     * @param  string  $model
     */
    #[Pure] public function __construct(string $model)
    {
        parent::__construct($model);
    }

    public function printModel(): string
    {
        return $this->model.PHP_EOL;
    }
}
