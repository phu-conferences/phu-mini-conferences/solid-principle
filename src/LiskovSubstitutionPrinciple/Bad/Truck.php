<?php

namespace App\LiskovSubstitutionPrinciple\Bad;

use JetBrains\PhpStorm\Pure;

class Truck extends Vehicle
{
    /**
     * @param  string  $model
     */
    #[Pure] public function __construct(string $model)
    {
        parent::__construct($model);
    }

    public function printModel()
    {
        return $this->model.PHP_EOL;
    }
}
