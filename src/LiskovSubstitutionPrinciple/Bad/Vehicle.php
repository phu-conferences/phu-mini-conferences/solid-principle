<?php

namespace App\LiskovSubstitutionPrinciple\Bad;

class Vehicle
{
    public function __construct(
        protected string $model
    ) {
    }

    public function printModel()
    {
        return $this->model.PHP_EOL;
    }
}
