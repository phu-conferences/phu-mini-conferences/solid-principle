<?php

namespace App\LiskovSubstitutionPrinciple\Bad;

require_once '../../../init.php';

echo 'First vehicles'.PHP_EOL;
$firstVehicles = [];
$firstVehicles[] = new Truck('1');
$firstVehicles[] = new Truck('2');
$firstVehicles[] = new Car('3');

foreach ($firstVehicles as $firstVehicle) {
    echo $firstVehicle->printModel();
}

echo '---------------'.PHP_EOL;

echo 'Second vehicles'.PHP_EOL;
$secondVehicles = [];
$secondVehicles[] = new Truck('1');
$secondVehicles[] = new Truck('2');
$secondVehicles[] = new Car('A43');

foreach ($secondVehicles as $secondVehicle) {
    echo $secondVehicle->printModel();
}
