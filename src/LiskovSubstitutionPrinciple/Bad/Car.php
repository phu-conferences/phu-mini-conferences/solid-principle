<?php

namespace App\LiskovSubstitutionPrinciple\Bad;

use JetBrains\PhpStorm\Pure;

class Car extends Vehicle
{
    /**
     * @param  string  $model
     */
    #[Pure] public function __construct(string $model)
    {
        parent::__construct($model);
    }

    public function printModel()
    {
        return (int)$this->model.PHP_EOL;
    }
}
