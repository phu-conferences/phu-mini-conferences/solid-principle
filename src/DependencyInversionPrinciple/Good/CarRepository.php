<?php

namespace App\DependencyInversionPrinciple\Good;

class CarRepository
{
    private DatabaseConnection $db;

    public function __construct(DatabaseConnection $db)
    {
        $this->db = $db;
        $this->db->connect();
    }

    public function __destruct()
    {
        $this->db->disconnect();
    }

    public function getAllCars()
    {
        $this->db->findAll();
    }
}
