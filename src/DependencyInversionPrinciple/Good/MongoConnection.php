<?php

namespace App\DependencyInversionPrinciple\Good;

class MongoConnection implements DatabaseConnection
{
    public function connect()
    {
        echo 'MONGO: Connect'.PHP_EOL;
    }

    public function disconnect()
    {
        echo 'MONGO: Disconnect'.PHP_EOL;
    }

    public function findAll()
    {
        echo 'MONGO: Find all'.PHP_EOL;
    }
}
