<?php

namespace App\DependencyInversionPrinciple\Good;

interface DatabaseConnection
{
    public function connect();

    public function disconnect();

    public function findAll();
}
