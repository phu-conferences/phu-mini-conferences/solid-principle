<?php

namespace App\DependencyInversionPrinciple\Good;

require_once '../../../init.php';

$mysqlConnection = new MySQLConnection();
$carRepository = new CarRepository($mysqlConnection);
$carRepository->getAllCars();

$mongoConnection = new MongoConnection();
$carRepository = new CarRepository($mongoConnection);
$carRepository->getAllCars();
