<?php

namespace App\DependencyInversionPrinciple\Good;

class MySQLConnection implements DatabaseConnection
{
    public function connect()
    {
        echo 'MYSQL: Connect'.PHP_EOL;
    }

    public function disconnect()
    {
        echo 'MYSQL: Disconnect'.PHP_EOL;
    }

    public function findAll()
    {
        echo 'MYSQL: Find all'.PHP_EOL;
    }
}
