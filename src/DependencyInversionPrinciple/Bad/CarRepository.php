<?php

namespace App\DependencyInversionPrinciple\Bad;

class CarRepository
{
    private MySQLConnection $db;

    public function __construct(MySQLConnection $db)
    {
        $this->db = $db;
        $this->db->connect();
    }

    public function __destruct()
    {
        $this->db->disconnect();
    }

    public function getAllCars()
    {
        $this->db->findAll();
    }
}
