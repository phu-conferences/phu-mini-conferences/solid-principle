<?php

namespace App\DependencyInversionPrinciple\Bad;

require_once '../../../init.php';

$mysqlConnection = new MySQLConnection();
$carRepository = new CarRepository($mysqlConnection);
$carRepository->getAllCars();

$mongoConnection = new MongoConnection();
$carRepository = new CarRepository($mongoConnection);
$carRepository->getAllCars();
