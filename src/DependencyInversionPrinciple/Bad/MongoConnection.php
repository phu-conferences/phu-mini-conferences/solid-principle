<?php

namespace App\DependencyInversionPrinciple\Bad;

class MongoConnection
{
    public function connect()
    {
        echo 'MONGO: Connect'.PHP_EOL;
    }

    public function disconnect()
    {
        echo 'MONGO: Disconnect'.PHP_EOL;
    }

    public function findAll()
    {
        echo 'MONGO: Find all'.PHP_EOL;
    }
}
