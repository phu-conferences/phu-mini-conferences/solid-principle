<?php

namespace App\OpenClosedPrinciple\Good;

class Car extends Vehicle
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'car';
    }

    public function __toString()
    {
        return 'Model: '.$this->model;
    }
}
