<?php

namespace App\OpenClosedPrinciple\Good;

class VehicleFactory
{
    public function __construct()
    {
    }

    public function produce(Vehicle $vehicle)
    {
        echo 'Produce '.$vehicle.' '.$vehicle->getType().PHP_EOL;
    }
}
