<?php

namespace App\OpenClosedPrinciple\Good;

class Truck extends Vehicle
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'truck';
    }

    public function __toString()
    {
        return 'Model: '.$this->model;
    }
}
