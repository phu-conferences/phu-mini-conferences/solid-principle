<?php

namespace App\OpenClosedPrinciple\Good;

abstract class Vehicle
{
    abstract function getType(): string;
}
