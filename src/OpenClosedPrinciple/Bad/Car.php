<?php

namespace App\OpenClosedPrinciple\Bad;

class Car extends Vehicle
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    public function __toString()
    {
        return 'Model: '.$this->model;
    }
}
