<?php

namespace App\OpenClosedPrinciple\Bad;

class VehicleFactory
{
    public function __construct()
    {
    }

    public function produce(Vehicle $vehicle)
    {
        $message = 'Produce '.$vehicle;
        if ($vehicle instanceof Car) {
            echo $message.' car'.PHP_EOL;
        } elseif ($vehicle instanceof Truck) {
            echo $message.' truck'.PHP_EOL;
        }
    }
}
