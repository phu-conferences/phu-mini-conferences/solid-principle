<?php

namespace App\OpenClosedPrinciple\Bad;

class Container extends Vehicle
{
    public function __construct(
        private string $model
    ) {
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'container';
    }

    public function __toString()
    {
        return 'Model: '.$this->model;
    }
}
