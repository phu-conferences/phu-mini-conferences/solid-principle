<?php

namespace App\OpenClosedPrinciple\Bad;

require '../../../init.php';

const CAR_MODEL_09_10_2021 = 'racing';
const TRUCK_MODEL_09_10_2021 = 'giant';
const CONTAINER_MODEL_09_10_2021 = 'monster';

$car = new Car(CAR_MODEL_09_10_2021);
$truck = new Truck(TRUCK_MODEL_09_10_2021);
$container = new Container(CONTAINER_MODEL_09_10_2021);

$vehicleFactory = new VehicleFactory();
$vehicleFactory->produce($car);
$vehicleFactory->produce($truck);
$vehicleFactory->produce($container);
