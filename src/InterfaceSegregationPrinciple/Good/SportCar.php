<?php

namespace App\InterfaceSegregationPrinciple\Good;

class SportCar implements Runnable
{
    public function run()
    {
        echo 'Car Run'.PHP_EOL;
    }
}
