<?php

namespace App\InterfaceSegregationPrinciple\Good;

interface Runnable
{
    function run();
}
