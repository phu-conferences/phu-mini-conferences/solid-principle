<?php

namespace App\InterfaceSegregationPrinciple\Good;

interface Flyable
{
    function fly();
}
