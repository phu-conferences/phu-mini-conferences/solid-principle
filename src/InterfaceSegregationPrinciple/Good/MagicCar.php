<?php

namespace App\InterfaceSegregationPrinciple\Good;

class MagicCar implements Flyable, Runnable
{
    public function run()
    {
        echo 'MagicCar Run'.PHP_EOL;
    }

    public function fly()
    {
        echo 'MagicCar Fly'.PHP_EOL;
    }
}
