<?php

namespace App\InterfaceSegregationPrinciple\Bad;

require_once '../../../init.php';

$sportCar = new SportCar();
$magicCar = new MagicCar();

$sportCar->fly();
$sportCar->run();

$magicCar->fly();
$magicCar->run();
