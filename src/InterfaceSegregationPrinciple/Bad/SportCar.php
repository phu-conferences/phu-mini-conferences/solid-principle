<?php

namespace App\InterfaceSegregationPrinciple\Bad;

class SportCar implements CarInterface
{
    public function run()
    {
        echo 'Car Run'.PHP_EOL;
    }

    public function fly()
    {
        // TODO: Implement fly() method.
    }
}
