<?php

namespace App\InterfaceSegregationPrinciple\Bad;

interface CarInterface
{
    public function run();

    public function fly();
}
