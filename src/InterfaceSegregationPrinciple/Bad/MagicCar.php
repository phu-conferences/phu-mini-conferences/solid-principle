<?php

namespace App\InterfaceSegregationPrinciple\Bad;

class MagicCar implements CarInterface
{
    public function run()
    {
        echo 'MagicCar Run'.PHP_EOL;
    }

    public function fly()
    {
        echo 'MagicCar Fly'.PHP_EOL;
    }
}
