# Feedback
General: If you do this, this is risk. If you apply principle, this is benefit.

## S: 
- why to use it?
- if have a problem then need to go to many places to find the problem, with S, it better
- if every component have less code, it easier to maintain to improve

## OC: 
- May add a risk, the code which is tested can be broken.
- May break another class use this class

## L:
- Enforce with interface
- Put parent type as example, so if child class not compatible, it has an error

## DI:
- Need more detail and benefit